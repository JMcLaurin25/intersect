
#ifndef TREE_ROTS_H
#define TREE_ROTS_H


int height(struct wd_node *t);
int get_balance(struct wd_node *t);

struct wd_node *rot_right(struct wd_node *t);	//Rotate to the right
struct wd_node *rot_left(struct wd_node *t);	//Rotate to the left

#endif
