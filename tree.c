
#include <stdlib.h>
#include <string.h>

#include "hash.h"
#include "tree.h"
#include "tree_rots.h"

//Creation functions
bst *bst_create(int (*cmp)(char *, const struct wd_node *))
{
	bst *bst = malloc(sizeof(*bst));
	if(bst) {
		bst->root = NULL;
		bst->cmp = cmp;
	}

	return bst;
}

//Modification functions
static struct wd_node *tree_create(char *word)
{
	struct wd_node *new_tree = malloc(sizeof(*new_tree));
	if (new_tree) {
		new_tree->word = strdup(word);
		new_tree->left = new_tree->right = NULL;
		new_tree->height = 1;//Initial height
	}

	return new_tree;
}

//Add the word to the tree
static struct wd_node *tree_insert(struct wd_node *cur_node, char *word, int (*cmp)(char *, const struct wd_node *))
{
	if (!cur_node) {
		return tree_create(word);
	}

	if (strcasecmp(word, cur_node->word) == 0) {
		return cur_node;
	}

	if (cmp(word, cur_node) < 0) {
		cur_node->left = tree_insert(cur_node->left, word, cmp);
	} else {
		cur_node->right = tree_insert(cur_node->right, word, cmp);
	}

	//AVL Arrangement logic troubleshooting/testing 
	//performed with SPC Samuels, Alexander.
	int left_ht, right_ht;
	left_ht = right_ht = 0;

	left_ht = cur_node->left ? cur_node->left->height : 0;
	right_ht = cur_node->right ? cur_node->right->height : 0;

	cur_node->height = (left_ht > right_ht) ? left_ht : right_ht + 1;

	int balance = get_balance(cur_node);
	int balance_child = 0;

	if (balance > 0) {
		balance_child = get_balance(cur_node->right);
	} else {
		balance_child = get_balance(cur_node->left);
	}
	//Check balance
	//Left-Left: Rotate parent left
	if (balance > 1 && balance_child >= 0) {
		return rot_left(cur_node);
	}

	//Right-Right: Rotate parent right
	if (balance < -1 && balance_child < 0) {
		return rot_right(cur_node);
	}

	//Left-Right: Rotate child Right, Parent Left
	if (balance > 1 && balance_child < 0) {
		cur_node->right = rot_right(cur_node->right);
		return rot_left(cur_node); 
	}

	//Right-Left: Rotate child Left, Parent Right
	if (balance < -1 && balance_child >= 0) {
		cur_node->left = rot_left(cur_node->left);
		return rot_right(cur_node);
	}
	return cur_node;
}

bool bst_insert(bst *cur_bst, char *word)
{
	if (!cur_bst) {
		return false;
	} else if (!cur_bst->root) {
		cur_bst->root = tree_create(word);
		return true;
	}

	//recursion begins in here
	cur_bst->root = tree_insert(cur_bst->root, word, cur_bst->cmp);
	return true;
}

void tree_destroy(struct wd_node *cur_tree)
{
	if (!cur_tree) {
		return;
	}
	if (cur_tree->left) {
		tree_destroy(cur_tree->left);
	}
	if (cur_tree->right) {
		tree_destroy(cur_tree->right);
	}
	free(cur_tree->word);
	free(cur_tree);
}

//Not used
void tree_disassemble(struct wd_node *cur_tree)
{
	if (!cur_tree) {
		return;
	}
	if (cur_tree->left) {
		tree_disassemble(cur_tree->left);
	}
	if (cur_tree->right) {
		tree_disassemble(cur_tree->right);
	}
	free(cur_tree);
}

void bst_destroy(bst *cur_bst)
{
	if (!cur_bst || !cur_bst->root) {
		return;
	}
	tree_destroy(cur_bst->root);
	free(cur_bst);
}

//Printing of data
void print_cur_tree_data(struct wd_node *t)
{
	if (!t) {
		return;
	}
	if (t->left) {
		print_cur_tree_data(t->left);
	}
	printf("%s\n", t->word);
	if (t->right) {
		print_cur_tree_data(t->right);
	}
}

void print_cur_bst_data(bst *cur_bst)
{
	if (!cur_bst) {
		return;
	}

	print_cur_tree_data(cur_bst->root);
}

//node comparator functions
int cmp_word(char *word, const struct wd_node *node)
{
	if (!word || !node->word) {
		return 0;
	}

	if (strcasecmp(node->word, word) < 0) {
		return 1;
	} else if (strcasecmp(node->word, word) > 0) {
		return -1;
	}
	return 0;
}

