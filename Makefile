
CFLAGS+=-std=c11 -D_GNU_SOURCE
CFLAGS+=-Wall -Wextra -Wpedantic
CFLAGS+=-Wwrite-strings -Wstack-usage=1024 -Wfloat-equal -Waggregate-return -Winline -fstack-usage

LDLIBS+=-lm

intersect: intersect.o hash.o tree.o tree_rots.o 

.PHONY: clean debug profile

clean:
	-rm *.o *.su intersect

debug: CFLAGS+=-g
debug: intersect

profile: CFLAGS+=-pg
profile: LDFLAGS+=-pg
profile: intersect
