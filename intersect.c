
#include <stdlib.h>
#include <string.h>

#include "hash.h"

int main(int argc, char *argv[])
{
	if (argc < 2) {
		printf("Provide File for input.\n");
		return 1;
	}

	char *word_buf = malloc(256);
	if (!word_buf) {
		return 1;
	}

	hash_table *main_hash = h_create();
	if (!main_hash) {
		free(word_buf);
		return 1;
	}		
	
	FILE *fp = fopen(argv[1], "r");
	if (!fp) {
		free(word_buf);
		printf("Error opening file!\n");
		return 1;
	}
		while (fscanf(fp, "%s", word_buf) != EOF) {
		h_insert(main_hash, word_buf);
		memset(word_buf, '\0', 256); 
	}
	fclose(fp);

	//Compare words from second file to words from first.
	if (argc > 1) {
		for (int index = 2; index < argc; index++) {	
			hash_table *alt_hash = h_create(); //Storage for matched words
			if (!alt_hash) {
				free(word_buf);
				h_destroy(main_hash);
				return 1;
			}		
		
			FILE *fp = fopen(argv[index], "r");
			if (!fp) {
				free(word_buf);
				h_destroy(main_hash);
				h_destroy(alt_hash);
				printf("Error opening file!\n");
				return 1;
			}

			while (fscanf(fp, "%s", word_buf) != EOF) {
				char *found;
				if ((found = h_fetch(main_hash, word_buf)) != NULL) {
					h_insert(alt_hash, found);
				}	
				memset(word_buf, '\0', 256); 
			}
	
			//Redirect pointer to new storage.
			hash_table *tmp;
			tmp = main_hash;
			h_destroy(tmp);

			main_hash = alt_hash;

			fclose(fp);
		}
	}

	free(word_buf);

	//Build ordered bst from hash_table
	bst *main_bst = bst_create(&cmp_word);
	if (!main_bst) {
		h_destroy(main_hash);
		return 1;
	}

	build_bst(main_bst, main_hash);

	h_destroy(main_hash);
	//output
	print_cur_bst_data(main_bst);
	bst_destroy(main_bst);
}
