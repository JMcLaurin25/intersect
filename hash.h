
#ifndef HASH_H
#define HASH_H

#include "tree.h"

struct hash_list {
	char *word;
	struct hash_list *next;
};

typedef struct {
	size_t count;
	size_t capacity;
	struct hash_list **data;
} hash_table;

hash_table *h_create(void);
void h_destroy(hash_table *h);

void h_insert(hash_table *h, char *word);
char *h_fetch(hash_table *h, char *word);

void h_print(hash_table *h);
void build_bst(bst *in_order, hash_table *h);

#endif
