
#include "hash.h"

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

static struct hash_list *hash_list_create(char *word);
static void hash_list_destroy(struct hash_list *hlist);
static size_t h_process(const char *word, size_t capacity);
static void h_resize(hash_table *h);

//Hash function. Currently using wanghash.
uint64_t wang_hash(uint64_t key)
{
	key = (~key) + (key << 21); // key = (key << 21) - key - 1;
	key = key ^ (key >> 24);
	key = (key + (key << 3)) + (key << 8); // key * 265
	key = key ^ (key >> 14);
	key = (key + (key << 2)) + (key << 4); // key * 21
	key = key ^ (key >> 28);
	key = key + (key << 31);
	return key;
}

/*
djb2 algorithm
First reported by Dan Bernstein

http://www.cse.yorku.ca/~oz/hash.html

size_t hash(unsigned char *str)
{
	size_t hash = 5381;
	int c;

	while (c = *str++) {
		hash = ((hash << 5) + hash) + c; // hash * 33 + c 
	}
	return hash;
}
*/

hash_table *__h_create(size_t capacity)
{
	hash_table *new_table = malloc(sizeof(*new_table));
	if (!new_table) {
		return NULL;
	}

	new_table->data = calloc(capacity, sizeof(*new_table->data));
	if (!new_table->data) {
		free(new_table);
		return NULL;
	}

	//Set variables.
	new_table->count = 0;
	new_table->capacity = capacity;

	return new_table;
}

hash_table *h_create(void)
{
	static size_t DEFAULT_CAPACITY = 256;
	return __h_create(DEFAULT_CAPACITY);
}

void h_destroy(hash_table *h)
{
	if (!h) {	
		return;
	}

	for (size_t index = 0; index < h->capacity; index++) {
		hash_list_destroy(h->data[index]);
	}

	free(h->data);
	free(h);
}

void h_insert(hash_table *h, char *word)
{
	if (!h || !word) {
		return;
	}

	//Verify capacity and resize if needed.
	h_resize(h);

	//1. run through h_process.
	size_t index = h_process(word, h->capacity);

	struct hash_list *is_copy = h->data[index];

	//Check for duplicate prior to entering new word.
	while (is_copy) {
		if (strcasecmp(is_copy->word, word) == 0) {
			return;
		}
		is_copy = is_copy->next;
	}

	struct hash_list *new_list = hash_list_create(word);
	if (!new_list) {
		return;
	}

	new_list->next = h->data[index];
	h->data[index] = new_list;
	h->count += 1;
}

char *h_fetch(hash_table *h, char *word)
{
	if (!h) {
		return NULL;
	}

	int index = h_process(word, h->capacity);

	struct hash_list *cur_list = h->data[index];

	while (cur_list) {
		if (strcasecmp(cur_list->word, word) == 0) {
			return cur_list->word;
		}

		cur_list = cur_list->next;
	}
	return NULL;
}

void h_print(hash_table *h)
{
	if (!h) {
		return;
	}
	for (size_t index = 0; index < h->capacity; index++) {
		struct hash_list *cur_list = h->data[index];
		while (cur_list) {
			printf("%s\n", cur_list->word);
			cur_list = cur_list->next;
		}
	}
}

void build_bst(bst *in_order, hash_table *h)
{
	if (!in_order || !h) {
		return;
	}

	for (size_t index = 0; index < h->capacity; index++) {
		struct hash_list *cur_list = h->data[index];
		while (cur_list) {
			bst_insert(in_order, cur_list->word);
			cur_list = cur_list->next;
		}
	}
}


//Static functions Here

static struct hash_list *hash_list_create(char *word)
{
	struct hash_list *new_list = malloc(sizeof(*new_list));
	if (!new_list) {
		return NULL;
	}

	new_list->word = strdup(word);
	new_list->next = NULL;

	return new_list;
}

static void hash_list_destroy(struct hash_list *hlist)
{
	while (hlist) {
		struct hash_list *temp = hlist->next;
		free(hlist->word);
		free(hlist);
		hlist = temp;
	}
}

static size_t h_process(const char *word, size_t capacity)
{
	//To utilize the wanghash, a uint64_t needs to be generated
	uint64_t buf = 0;
	strncpy((char *)(&buf), word, sizeof(buf));

	//Modulo of capacity to designate an existing index.
	return wang_hash(buf) % capacity;
}

//Resize hash table if capacity reaches 70%
static void h_resize(hash_table *h)
{
	if (!h) {
		return;
	}

	//Determine if capacity is at 70%
	if (h->count < 0.75 * h->capacity) {
		return;
	}

	//Create new array and transfer existing data to it.
	hash_table *new_table = __h_create(h->capacity * 2);
	if (!new_table) {
		return;
	}

	for (size_t index = 0; index < h->capacity; index++) {
		struct hash_list *temp = h->data[index];
		while (temp) {
			h_insert(new_table, temp->word);
			temp = temp->next;
		}
	}

	//with all data copied to new table. Destroy old table
	for (size_t index = 0; index < h->capacity; index++) {
		hash_list_destroy(h->data[index]);		
	}
	
	free(h->data);

	//Copy variables from replacement table
	h->count = new_table->count;
	h->capacity = new_table->capacity;
	h->data = new_table->data;
	free(new_table);
}

