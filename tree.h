
#ifndef TREE_H
#define TREE_H

#include <stdio.h>
#include <stdbool.h>

struct wd_node {
	char *word;
	struct wd_node *left, *right;
	int height;
};

typedef struct {
	struct wd_node *root;
	int (*cmp)(char *, const struct wd_node *);
} bst;

//Creation functions
bst *bst_create(int (*cmp)(char *, const struct wd_node *));

//Modification functions
bool bst_insert(bst *cur_bst, char *new_word);

//Break-down stage
void tree_destroy(struct wd_node *cur_tree);
void tree_disassemble(struct wd_node *cur_tree);
void bst_destroy(bst *cur_bst);

//Printing of data
void print_cur_tree_data(struct wd_node *t);
void print_cur_bst_data(bst *cur_bst);

//node comparator functions
int cmp_word(char *word, const struct wd_node *node);

#endif
