
#include "tree.h"
#include "tree_rots.h"

//Return height. If it doesn't exist return 0.
int height(struct wd_node *t)
{
	if (!t) {
		return 0;
	}
	return t->height;
}

//Return Balance. If it doesn't exist return 0.
int get_balance(struct wd_node *t)
{
	if (!t) {
		return 0;
	}
	//return height(t->right) - height(t->left);
	int left_ht, right_ht;
	left_ht = right_ht = 0;

	left_ht = t->left ? t->left->height : 0;
	right_ht = t->right ? t->right->height : 0;

	return right_ht - left_ht;
}

//Rotate to the right
struct wd_node *rot_right(struct wd_node *t)
{
	if (!t) {
		return NULL;
	}

	struct wd_node *old_left = t->left;
	struct wd_node *tmp = old_left->right;

	//Rotate pointers
	old_left->right = t;
	t->left = tmp;


	//Height update
	int left_ht, right_ht;
	left_ht = right_ht = 0;

	left_ht = t->left ? t->left->height : 0;
	right_ht = t->right ? t->right->height : 0;
	t->height = (left_ht > right_ht ? left_ht : right_ht) + 1;

	left_ht = old_left->left ? old_left->left->height : 0;
	right_ht = old_left->right ? old_left->right->height : 0;
	old_left->height = (left_ht > right_ht ? left_ht : right_ht) + 1;

	//Reset the root
	return old_left;
}

//Rotate to the left
struct wd_node *rot_left(struct wd_node *t)
{
	if (!t) {
		return NULL;
	}

	struct wd_node *old_right = t->right;
	struct wd_node *tmp = old_right->left;
 
	//Rotates pointers
	old_right->left = t;
	t->right = tmp;
 
	//Height update
	int left_ht, right_ht;
	left_ht = right_ht = 0;

	left_ht = t->left ? t->left->height : 0;
	right_ht = t->right ? t->right->height : 0;
	t->height = (left_ht > right_ht ? left_ht : right_ht) + 1;

	left_ht = old_right->left ? old_right->left->height : 0;
	right_ht = old_right->right ? old_right->right->height : 0;
	old_right->height = (left_ht > right_ht ? left_ht : right_ht) + 1;
 
	//Reset the root
	return old_right;
}
